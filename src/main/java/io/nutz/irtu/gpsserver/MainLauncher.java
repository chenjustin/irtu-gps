package io.nutz.irtu.gpsserver;

import org.nutz.boot.NbApp;
import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import io.nutz.irtu.gpsserver.bean.DevInfo;
import io.nutz.irtu.gpsserver.bean.DevLocMsg;
import io.nutz.irtu.gpsserver.bean.User;

// 主启动类,带main方法
@IocBean(create="init", depose="depose")
public class MainLauncher {
    
    // 注入配置信息
    @Inject
    protected PropertiesProxy conf;
    // 注入dao
    @Inject
    protected Dao dao;
    // 注入Ioc实例
    @Inject
    protected Ioc ioc;
    @At("/")
    @Ok("->:/index.html")
    public void index() {}
    
    public void init() {
        // 自动建表
        Daos.createTablesInPackage(dao, User.class, false);
        Daos.migration(dao, DevInfo.class, true, false);
        Daos.migration(dao, DevLocMsg.class, true, false);
        // 创建一个根用户,如果没有任何用户的话
        if (dao.count(User.class) == 0) {
            User user = new User();
            user.setName("wendal");
            user.setAge(18);
            user.setLocation("广州");
            dao.insert(user);
         }
        // 启动GPS服务器,监听19002端口
        ioc.get(GpsServer.class);
    }
    public void depose() {}

    public static void main(String[] args) throws Exception {
        new NbApp().setArgs(args).setPrintProcDoc(true).run();
    }

}
