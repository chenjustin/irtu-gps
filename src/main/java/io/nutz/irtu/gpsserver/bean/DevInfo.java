package io.nutz.irtu.gpsserver.bean;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.interceptor.annotation.PrevInsert;
import org.nutz.dao.interceptor.annotation.PrevUpdate;

@Table("t_dev")
public class DevInfo {

    // 设备IMEI
    @Name
    protected String imei;
    // SIM的iccid
    @Column
    protected String iccid;
    // 用户自定义的昵称
    @Column
    protected String nickname;
    // 归属用户
    @Column
    protected long userId;
    // 用户自定义标签,逗号分隔
    @Column
    protected String tags;
    // 设备创建时间
    @PrevInsert(now = true)
    @Column
    protected long createTime;
    // 设备信息更新时间
    @PrevInsert(now = true)
    @PrevUpdate(now = true)
    @Column
    protected long updateTime;
    // 最后连接上的session的id
    @Column("sesid")
    @ColDefine(width=32)
    protected String sessionID;
    
    // --------------------------------
    // 非数据库字段
    //----------------------------------
    // 设备最新状态
    @One(field="imei", key="imei")
    protected DevStatusMsg sta;
    // 设备最新位置
    protected DevLocMsg loc;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public DevStatusMsg getSta() {
        return sta;
    }

    public void setSta(DevStatusMsg sta) {
        this.sta = sta;
    }

    public DevLocMsg getLoc() {
        return loc;
    }

    public void setLoc(DevLocMsg loc) {
        this.loc = loc;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

}
